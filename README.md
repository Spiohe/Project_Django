Installation de virtualenv à faire (si ce n'est pas déjà fait).

Lancez votre virtualenv et allez dans ./Project_Django/djangoCompany

Réalisez les installations suivantes :
pip install django
pip install requests
pip install templated-docs

Il va falloir générer le fichier main.css, nous avons utiliser less pour nous faciliter le travail au niveau du css.
Pour cela, entrez :
npm install gulp
npm install gulp-less
npm install gulp-clean-css
npm install gulp-rename

Note : Mettre -g en plus pour l'ajouter en global, il est possible que le virtualenv soit activé lors des npm install pour que celui-ci s'installe correctement.

Par la suite, réalisez la commande : gulp dev

Le css est bel et bien généré !

Dernièe étape, génération de la base de donnée :

Dans ./Project_Django/djangoCompany
./manage.py makemigrations lesCompagnies
./manage.py sqlmigrate lesCompagnies 0001
./manage.py migrate
./manage.py loaddata stages.json (c'est possible que ça soit un peu plus voir très long que d'habitude à la suite de la surchage de la méthode init pour entreprise)

Note : Il faut à tout prix générer la base de donnée si db.sqlite3 n'est pas disponible dans le rendu, des fonctionnalités marchent à la suite de la mise à jour de longitude et de latitude.

Lancement du site :
./manage.py runserver

URL : http://localhost:8000
