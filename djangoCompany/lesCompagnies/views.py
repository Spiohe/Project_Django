from django.shortcuts import render, render_to_response,get_object_or_404,redirect
from django.http import HttpResponse, HttpResponseRedirect
from lesCompagnies.models import Entreprise, Contact, Taxe, Contacter
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from lesCompagnies.forms import FormulaireEntreprise, FormulaireContact, FormulaireTaxe, FormulaireContacter, FormulaireRechercheEntreprise,FormulaireRechercheContact
from templated_docs import fill_template
from templated_docs.http import FileResponse
import urllib.parse
import json, requests


# Entreprise ===============================================>

# Permet de la consultation de la liste des entreprises
# request : variable nécessaire pour la construction d'une page
# @return : un template (lesCompagnies/consultationEntreprise.html)
# plusieurs try catch mis en place pour controler la pagination
def consultationEntreprise(request):
    entreprises_liste=Entreprise.objects.all().order_by('nom')
    recherche=FormulaireRechercheEntreprise(data=request.POST)
    if recherche.is_valid():
        data = recherche.cleaned_data
        request.session['choixRechercheEntreprise'] = data['choixRecherche']
        request.session['champRechercheEntreprise'] = data['champRecherche']
        return redirect(rechercheEntreprise)
    paginator=Paginator(entreprises_liste,10)
    page=request.GET.get('page')
    try:
        entreprises=paginator.page(page)
    except PageNotAnInteger:
        entreprises=paginator.page(1)
    except EmptyPage:
        entreprises=paginator.page(paginator.num_pages)
    return render(request,'lesCompagnies/consultationEntreprise.html', {'entreprises':entreprises,'form':recherche})

# Permet la consultation de la liste des entreprises recherchées en fonction du critère entré
# request : variable nécessaire pour la construction d'une page
# @return : un template (lesCompagnies/rechercheEntreprise.html) avec des paramètres pour le formatage de la page
# redirection vers la méthode consultationEntreprise si echec
def rechercheEntreprise(request):
    recherche=FormulaireRechercheEntreprise(data=request.POST)
    choixRecherche = request.session['choixRechercheEntreprise']
    champRecherche = request.session['champRechercheEntreprise']
    if(choixRecherche == "nomEntreprise"):
        entreprises_liste=Entreprise.objects.filter(nom__icontains = champRecherche)
        phraseRecherche = "Résultat pour la recherche par nom de l'entreprise de"
    elif(choixRecherche == "villeEntreprise"):
        entreprises_liste=Entreprise.objects.filter(ville__icontains = champRecherche)
        phraseRecherche = "Résultat pour la recherche par ville de"
    elif(choixRecherche == "adresseEntreprise"):
        entreprises_liste=Entreprise.objects.filter(adresse__icontains = champRecherche)
        phraseRecherche = "Résultat pour la recherche par adresse de"
    elif(choixRecherche == "codePostalEntreprise"):
        entreprises_liste=Entreprise.objects.filter(codePostal__icontains = champRecherche)
        phraseRecherche = "Résultat pour la recherche par code postal de"
    elif(choixRecherche == "paysEntreprise"):
        entreprises_liste=Entreprise.objects.filter(pays__icontains = champRecherche)
        phraseRecherche = "Résultat pour la recherche par pays de"
    elif(choixRecherche == "numeroTelephoneEntreprise"):
        entreprises_liste=Entreprise.objects.filter(telephone__icontains = champRecherche)
        phraseRecherche = "Résultat pour la recherche par téléphone de"
    else:
        return redirect(consultationEntreprise)
    paginator=Paginator(entreprises_liste,10)
    page=request.GET.get('page')
    try:
        entreprises=paginator.page(page)
    except PageNotAnInteger:
        entreprises=paginator.page(1)
    except EmptyPage:
        entreprises=paginator.page(paginator.num_pages)
    return render(request,'lesCompagnies/rechercheEntreprise.html', {'entreprises':entreprises,'phraseRecherche':phraseRecherche,'champRecherche':champRecherche})

# Permet la consultation de consulter une seule entreprise et d'obtenir l'ensemble de ses infos (cartes, taxes, contacts, informations, contact pour taxe)
# request : variable nécessaire pour la construction d'une page
# identifiant : identifiant de l'entreprise qu'on souhaite consulter
# @return : un template (lesCompagnies/lireEntreprise.html) avec des paramètres pour le formatage de la page
# Redirection vers la méthode consultationEntreprise si echec
def lireEntreprise(request,identifiant):
    entreprise=Entreprise.objects.get(id=identifiant)
    request.session['identifiantEntreprise'] = identifiant
    contacts=Contact.objects.select_related().filter(entrepriseContact = identifiant)
    taxe=Taxe.objects.select_related().filter(entrepriseTaxe = identifiant).order_by('-dateTaxe')
    contacters=Contacter.objects.select_related().filter(entrepriseContacter = identifiant)
    try:
        longitude,latitude=getLatitudeLongitude(identifiant)
        return render_to_response('lesCompagnies/lireEntreprise.html', {'entreprise':entreprise,'longitude':longitude,'latitude':latitude,'contacts':contacts, 'taxes':taxe, 'contacters':contacters})
    except:
        return redirect(consultationEntreprise)

# Permet la suppression d'une entreprise
# request : variable nécessaire pour la construction d'une page
# idenfiaint : identifiant de l'entreprise à supprimer
# @return : redireciton vers la méthode consultationEntreprise
def suppressionEntreprise(request,identifiant):
    try:
        Entreprise.objects.get(id=identifiant).delete()
        return redirect(consultationEntreprise)
    except Entreprise.DoesNotExist:
        return redirect(consultationEntreprise)

# Permet l'ajout d'une entreprise
# request : variable nécessaire pour la construction d'une page
# @return : un template (lesCompagnies/ajouterEntreprise.html) avec des paramètres pour le formatage de la page
# lorsque le formulaire est validé on sauvegarde les informations et on redirige vers la méthode lireEntreprise avec l'id de l'entreprise
def ajouterEntreprise(request):
    form=FormulaireEntreprise()
    if request.method == "GET":
        return render(request, 'lesCompagnies/ajouterEntreprise.html', {'form': form})
    else:
        form = FormulaireEntreprise(request.POST)
        if form.is_valid():
            cont=form.save()
            cont.latitude, cont.longitude = getLatitudeLongitude(cont.id)
            cont.save()
            return redirect(lireEntreprise, identifiant=identifiant)
    return render(request,'lesCompagnies/ajouterEntreprise.html', {'form': form})

# Permet la modification d'une entreprise
# request : variable nécessaire pour la construction d'une page
# idenfiant : identifiant de l'entreprise à modifier
# @return : un template (lesCompagnies/modifierEntreprise.html) avec des paramètres pour le formatage de la page ainsi qu'un formulaire
# lorsque le formulaire est validé on sauvegarde les informations et on redirige vers la méthode consultationEntreprise
def modifierEntreprise(request,identifiant):
    try:
        entreprise=Entreprise.objects.get(id=identifiant)
        form=FormulaireEntreprise(instance=entreprise)
    except Entreprise.DoesNotExist:
        return redirect(consultationEntreprise)
    if request.method == "GET":
        return render(request, 'lesCompagnies/modifierEntreprise.html', {'form': form,'entreprise':entreprise})
    else:
        try:
            form = FormulaireEntreprise(request.POST,instance=entreprise)
        except Entreprise.DoesNotExist:
            return redirect(consultationEntreprise)
        if form.is_valid():
            cont=form.save()
            cont.latitude, cont.longitude = getLatitudeLongitude(cont.id)
            cont.save()
            return redirect(lireEntreprise, identifiant=identifiant)
    return render(request,'lesCompagnies/modifierEntreprise.html', {'form': form,'entreprise':entreprise})

# Permet la mise en place d'un itinéraire entre une entreprise, l'iut ou une autre entreprise
# request : variable nécessaire pour la construction d'une page
# @return : un template (lesCompagnies/itineraireEntreprise.html) avec des paramètres pour le formatage de la page
def itineraireEntreprise(request):
    entreprises=Entreprise.objects.all().order_by('nom')
    return render_to_response('lesCompagnies/itineraireEntreprise.html', {'entreprises':entreprises})

# Méthode permettant de récupérer la latitude et longitude en fonction de l'api google et des coordonnées dans l'entreprise
# idenfiant : identifiant de l'entreprise à modifier
# @return : longitude et latitude de l'entreprise
#  (0,0) de renvoyer si l'api renvoie rien
def getLatitudeLongitude(identifiant):
    entreprise=Entreprise.objects.get(id=identifiant)
    adressePropre=entreprise.adresse+","+entreprise.ville+","+entreprise.pays
    adressePropre=urllib.parse.quote(adressePropre)
    geocode_url = "http://maps.googleapis.com/maps/api/geocode/json?address="+adressePropre
    req = requests.get(geocode_url)
    data = json.loads(req.text)
    try:
        longitude=data['results'][0]['geometry']['location']['lng']
        latitude=data['results'][0]['geometry']['location']['lat']
    except:
        longitude, latitude = (0,0)
    return (longitude, latitude)


#Contact ===============================================>

# Permet la consultation d'un contact (affiche la liste complète des contacts)
# request : variable nécessaire pour la construction d'une page
# @return : un template (lesCompagnies/consultationContact.html) avec des paramètres pour le formatage de la page ainsi qu'un formulaire de recherche
# différents try, catch pour le controle de la pagination
# lorsque le formulaire est validé on sauvegarde les informations et on redirige vers la méthode rechercheContact (formulaire de recherche)
def consultationContact(request):
    contacts_liste=Contact.objects.all()
    recherche=FormulaireRechercheContact(data=request.POST)
    if recherche.is_valid():
        data = recherche.cleaned_data
        request.session['choixRechercheContact'] = data['choixRecherche']
        request.session['champRechercheContact'] = data['champRecherche']
        print(data['choixRecherche']+" "+data['champRecherche'])
        return redirect(rechercheContact)
    paginator=Paginator(contacts_liste,10)
    page=request.GET.get('page')
    try:
        contacts=paginator.page(page)
    except PageNotAnInteger:
        contacts=paginator.page(1)
    except EmptyPage:
        contacts=paginator.page(paginator.num_pages)
    return render(request,'lesCompagnies/consultationContact.html', {'contacts':contacts,'form':recherche})

# Permet la consultation de la liste des contacts recherchées en fonction du critère entré
# request : variable nécessaire pour la construction d'une page
# @return : un template (lesCompagnies/rechercheContact.html) avec des paramètres pour le formatage de la page
# redirection vers la méthode consultationContact si echec
def rechercheContact(request):
    choixRecherche = request.session['choixRechercheContact']
    champRecherche = request.session['champRechercheContact']
    if(choixRecherche == "entrepriseContact"):
        contacts_liste=Contact.objects.filter(entrepriseContact__nom__icontains = champRecherche)
        phraseRecherche = "Résultat pour la recherche par nom de l'entreprise de"
    elif(choixRecherche == "nomContact"):
        contacts_liste=Contact.objects.filter(nomContact__icontains = champRecherche)
        phraseRecherche = "Résultat pour la recherche par ville de"
    elif(choixRecherche == "libelleContact"):
        contacts_liste=Contact.objects.filter(libelleContact__icontains = champRecherche)
        phraseRecherche = "Résultat pour la recherche par adresse de"
    elif(choixRecherche == "emailContact"):
        contacts_liste=Contact.objects.filter(emailContact__icontains = champRecherche)
        phraseRecherche = "Résultat pour la recherche par code postal de"
    else:
        return redirect(consultationContact)
    paginator=Paginator(contacts_liste,10)
    page=request.GET.get('page')
    try:
        contacts=paginator.page(page)
    except PageNotAnInteger:
        contacts=paginator.page(1)
    except EmptyPage:
        contacts=paginator.page(paginator.num_pages)
    return render(request,'lesCompagnies/rechercheContact.html', {'contacts':contacts,'phraseRecherche':phraseRecherche,'champRecherche':champRecherche})

# Permet l'ajout d'un contact
# request : variable nécessaire pour la construction d'une page
# @return : un template (lesCompagnies/ajouterContact.html) avec des paramètres pour le formatage de la page
# lorsque le formulaire est validé on sauvegarde les informations et on redirige vers la méthode consultationContact
def ajouterContact(request):
    form=FormulaireContact()
    if request.method == "GET":
        return render(request, 'lesCompagnies/ajouterContact.html', {'form': form})
    else:
        form = FormulaireContact(request.POST)
        if form.is_valid():
            cont=form.save()
            cont.save()
            return redirect(consultationContact)
    return render(request,'lesCompagnies/ajouterContact.html', {'form': form})

# Permet la modification d'un contact
# request : variable nécessaire pour la construction d'une page
# idenfiant : identifiant du contact à modifier
# @return : un template (lesCompagnies/modifierContact.html) avec des paramètres pour le formatage de la page ainsi qu'un formulaire
# lorsque le formulaire est validé on sauvegarde les informations et on redirige vers la méthode index (pb de rediection directement sur lireEntreprise)
# Redirection vers index si le contact existe pas
def modifierContact(request,identifiant):
    try:
        contact=Contact.objects.get(id=identifiant)
        form=FormulaireContact(instance=contact)
    except Contact.DoesNotExist:
        return redirect(index)
    if request.method == "GET":
        return render(request, 'lesCompagnies/modifierContact.html', {'form': form,'contact':contact})
    else:
        try:
            form = FormulaireContact(request.POST,instance=contact)
        except Contact.DoesNotExist:
            return redirect(index)
        if form.is_valid():
            cont=form.save()
            cont.save()
            return redirect(index)
    return render(request,'lesCompagnies/modifierContact.html', {'form': form,'contact':contact})

# Permet la suppression d'un contact
# request : variable nécessaire pour la construction d'une page
# idenfiaint : identifiant du contact à supprimer
# @return : redirection vers la méthode consultationEntreprise
def suppressionContact(request,identifiant):
    try:
        Contact.objects.get(id=identifiant).delete()
        return redirect(consultationEntreprise)
    except Contact.DoesNotExist:
        return redirect(consultationEntreprise)

# Permet l'ajout d'un contact ditectement en fonction de l'entreprise (met directement la bonne entreprise dans le champ select)
# request : variable nécessaire pour la construction d'une page
# identifiant : identifiant de l'entreprise
# @return : un template (lesCompagnies/ajouterContact.html) avec des paramètres pour le formatage de la page
# lorsque le formulaire est validé on sauvegarde les informations et on redirige vers la méthode lireEntreprise avec l'id de l'entreprise
def ajouterContactParEntreprise(request,identifiant):
    form=FormulaireContact(initial={'entrepriseContact':identifiant})
    if request.method == "GET":
        return render(request, 'lesCompagnies/ajouterContact.html', {'form': form})
    else:
        form = FormulaireContact(request.POST)
        if form.is_valid():
            cont=form.save()
            cont.save()
            return redirect(lireEntreprise, identifiant=identifiant)
    return render(request,'lesCompagnies/ajouterContact.html', {'form': form})


#Index ===============================================>

# Affichage de l'index
# Request : variable nécessaire pour la construction d'une page
# @Return : un template (lesCompagnies/pageAccueil.html)
def index(request):
    lastid=Entreprise.objects.latest('id').id # Récupère l'identifiant de la dernière entreprise crée
    lastids=lastid-10 # Remonte l'id de 10
    entreprises=Entreprise.objects.filter(id__gt=lastids).order_by('-id') # Récupère les 10 dernières entreprises crées

    longitude,latitude=getLatitudeLongitude(lastid)

    return render_to_response('lesCompagnies/pageAccueil.html', {'entreprises':entreprises,'longitude':longitude,'latitude':latitude} )


#Taxe =================================================>

# Ajout d'une taxe
# Request : variable nécessaire pour la construction d'une page
# @Return : un template (lesCompagnies/ajouterTaxe.html)
def ajouterTaxe(request):
    form=FormulaireTaxe()
    if request.method == "GET":
        return render(request, 'lesCompagnies/ajouterTaxe.html', {'form': form})
    else:
        form = FormulaireTaxe(request.POST)
        if form.is_valid():
            cont=form.save()
            cont.save()
            return redirect(index)
    return render(request,'lesCompagnies/ajouterTaxe.html', {'form': form})

# Ajout d'une taxe par rapport à l'id de l'entreprise
# Request : variable nécessaire pour la construction d'une page
# Identifiant : identifiant de l'entreprise
# @Return : un template (lesCompagnies/ajouterTaxe.html)
def ajouterTaxeParEntreprise(request,identifiant):
    form=FormulaireTaxe(initial={'entrepriseTaxe':identifiant}) #Envoie au formulaire l'identifiant de l'entreprise
    if request.method == "GET":
        return render(request, 'lesCompagnies/ajouterTaxe.html', {'form': form})
    else:
        form = FormulaireTaxe(request.POST)
        if form.is_valid():
            cont=form.save()
            cont.save()
            return redirect(lireEntreprise, identifiant=identifiant)
    return render(request,'lesCompagnies/ajouterTaxe.html', {'form': form})

# Modification d'une taxe
# Request : variable nécessaire pour la construction d'une page
# Identifiant : identifiant de la taxe
# @Return : un template (lesCompagnies/modifierTaxe.html)
def modifierTaxe(request,identifiant):
    try:
        taxe=Taxe.objects.get(id=identifiant)
        form=FormulaireTaxe(instance=taxe)
    except Taxe.DoesNotExist:
        return redirect(index)
    if request.method == "GET":
        return render(request, 'lesCompagnies/modifierTaxe.html', {'form': form,'taxe':taxe})
    else:
        try:
            form = FormulaireTaxe(request.POST,instance=taxe)
        except Taxe.DoesNotExist:
            return redirect(index)
        if form.is_valid():
            cont=form.save()
            cont.save()
            return redirect(consultationEntreprise)
    return render(request,'lesCompagnies/modifierTaxe.html', {'form': form,'taxe':taxe})

# Suppression d'une taxe
# Request : variable nécessaire pour la construction d'une page
# Identifiant : identifiant de la taxe
# @Return : un template (consultationEntreprise)
def suppressionTaxe(request,identifiant):
    try:
        Taxe.objects.get(id=identifiant).delete()
        return redirect(consultationEntreprise)
    except Taxe.DoesNotExist:
        return redirect(consultationEntreprise)

#Contacter ====================================================>

# Ajout d'un contacter par rapport à l'id de l'entreprise
# Request : variable nécessaire pour la construction d'une page
# Identifiant : identifiant de l'entreprise
# @Return : un template (lesCompagnies/modifierContacter.html)
def ajouterContacterParEntreprise (request,identifiant):
    form=FormulaireContacter(initial={'entrepriseContacter':identifiant}, leContacter=identifiant)
    if request.method == "GET":
        return render(request, 'lesCompagnies/ajouterContacterParEntreprise.html', {'form': form})
    else:
        form = FormulaireContacter(request.POST,initial={'entrepriseContacter':identifiant}, leContacter=identifiant)
        if form.is_valid():
            cont=form.save()
            cont.save()
            return redirect(lireEntreprise, identifiant=identifiant)
    return render(request,'lesCompagnies/ajouterContacterParEntreprise.html', {'form': form})

# Modification d'un contacter
# Request : variable nécessaire pour la construction d'une page
# Identifiant : identifiant du Contacter
# @Return : un template (lesCompagnies/ajouterContacterParEntreprise.html) avec un paramètre pour le formatage de la page
def modifierContacter(request,identifiant):
    try:
        identifiantEntreprise=request.session['identifiantEntreprise']
        contacter=Contacter.objects.get(id=identifiant)
        form=FormulaireContacter(instance=contacter,leContacter=identifiantEntreprise)
    except Taxe.DoesNotExist:
        return redirect(index)
    if request.method == "GET":
        return render(request, 'lesCompagnies/modifierContacter.html', {'form': form,'contacter':contacter})
    else:
        try:
            form = FormulaireContacter(request.POST,instance=contacter, leContacter=identifiantEntreprise)
        except Contacter.DoesNotExist:
            return redirect(index)
        if form.is_valid():
            cont=form.save()
            cont.save()
            return redirect(consultationEntreprise)
    return render(request,'lesCompagnies/modifierContacter.html', {'form': form,'contacter':contacter})

# Suppression d'un contacter
# Request : variable nécessaire pour la construction d'une page
# Identifiant : identifiant du Contacter
# @Return : redirige vers le template lireEntreprise
def suppressionContacter(request,identifiant): 
    try:
        Contacter.objects.get(id=identifiant).delete()
        return redirect(consultationEntreprise)
    except Contacter.DoesNotExist:
        return redirect(consultationEntreprise)


#Affichage du message
# Request : variable nécessaire pour la construction d'une page
# Identifiant : identifiant de le contacter
# @Return :
def publipostage(request,identifiant):
    contacter=Contacter.objects.get(id=identifiant)
    context = {'contacter': contacter}
    filename = fill_template('lesCompagnies/fichierODT.odt', context, output_format='odt')   #Utiliser setattr ?
    visible_filename = 'fichierODT.odt'
    return FileResponse(filename, visible_filename)
