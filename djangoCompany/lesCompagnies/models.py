from django.db import models
from django.utils import timezone
import datetime
import re
import urllib.parse
import json, requests


RE_CEDEX = re.compile(r'(.*)\b\s*cedex.*$',re.IGNORECASE|re.MULTILINE)
RE_CEDEX = re.compile(r'(.*)\b\s*bp.*$',re.IGNORECASE|re.MULTILINE)


# Table Pays (on s'en sert pas à cause de problèmes de performances)
class Pays(models.Model):
    code = models.PositiveSmallIntegerField(unique=True)
    alpha2 = models.CharField(max_length=2,unique=True)
    alpha3 = models.CharField(max_length=3,unique=True)
    nom_en_gb = models.CharField(max_length=45)
    nom_fr_fr = models.CharField(max_length=45)

    def __str__(self):
        return self.nom_fr_fr

# Table Ville de France (on s'en sert pas à cause de problèmes de performances)
class VillesDeFrance(models.Model):
    id                           = models.AutoField(primary_key=True, db_column="ville_id")
    departement                  = models.CharField(max_length=3,     db_column="ville_departement")
    slug                         = models.CharField(max_length=255,   db_column="ville_slug")
    nom                          = models.CharField(max_length=45,    db_column="ville_nom")
    nom_reel                     = models.CharField(max_length=45,    db_column="ville_nom_reel")
    nom_soundex                  = models.CharField(max_length=45,    db_column="ville_nom_soundex")
    nom_metaphone                = models.CharField(max_length=45,    db_column="ville_nom_metaphone")
    code_postal                  = models.CharField(max_length=255,   db_column="ville_code_postal")
    commune                      = models.CharField(max_length=3,     db_column="ville_commune")
    code_commune                 = models.CharField(max_length=5,     db_column="ville_code_commune")
    arrondissement               = models.PositiveSmallIntegerField(  db_column="ville_arrondissement")
    canton                       = models.CharField(max_length=4,     db_column="ville_canton")
    amdi                         = models.PositiveSmallIntegerField(  db_column="ville_amdi")
    population_2010              = models.PositiveIntegerField(       db_column="ville_population_2010")
    population_1999              = models.PositiveIntegerField(       db_column="ville_population_1999")
    population_2012              = models.PositiveIntegerField(       db_column="ville_population_2012")
    densite_2010                 = models.IntegerField(               db_column="ville_densite_2010")
    surface                      = models.PositiveIntegerField(       db_column="ville_surface")
    longitude_deg                = models.FloatField(                 db_column="ville_longitude_deg")
    latitude_deg                 = models.FloatField(                 db_column="ville_latitude_deg")
    longitude_grd                = models.CharField(max_length=9,     db_column="ville_longitude_grd")
    latitude_grd                 = models.CharField(max_length=8,     db_column="ville_latitude_grd")
    longitude_dms                = models.CharField(max_length=9,     db_column="ville_longitude_dms")
    latitude_dms                 = models.CharField(max_length=8,     db_column="ville_latitude_dms")
    zmin                         = models.IntegerField(               db_column="ville_zmin")
    zmax                         = models.IntegerField(               db_column="ville_zmax")
    population_2010_order_france = models.IntegerField(               db_column="ville_population_2010_order_france")
    densite_2010_order_france    = models.IntegerField(               db_column="ville_densite_2010_order_france")
    surface_order_france         = models.IntegerField(               db_column="ville_surface_order_france")

    class Meta:
        db_table = "villes_france"

    def __str__(self):
        return self.nom_reel

# Méthode permettant de remplacer automatiquement les entreprises dont la longitude et latitude était resté à null à cause de l'ancienne version
def getLatitudeLongitude(adresse):
    geocode_url = "http://maps.googleapis.com/maps/api/geocode/json?address="+adresse
    req = requests.get(geocode_url)
    data = json.loads(req.text)
    try:
        longitude=data['results'][0]['geometry']['location']['lng']
        latitude=data['results'][0]['geometry']['location']['lat']
    except:
        longitude, latitude = (0.1,0.1)
    return (longitude, latitude)

# Table entreprise
class Entreprise(models.Model):
    nom        = models.CharField(max_length=100)
    adresse    = models.CharField(max_length=250)
    codePostal = models.CharField(max_length=10)
    ville      = models.CharField(max_length=50)
    pays       = models.CharField(max_length=30)
    telephone  = models.CharField(max_length=20)
    latitude   = models.DecimalField(max_digits=13, decimal_places=10)
    longitude  = models.DecimalField(max_digits=13, decimal_places=10)
    # __init__ va remplacer et générer une adresse pour trouver la longitude et latitude et ensuite la sauvegarder
    def __init__(self,*args,**kwargs):
        super(Entreprise,self).__init__(*args,**kwargs)
        if (self.latitude==0 and self.longitude==0):
            adresse = self.adresse+","+self.ville+","+self.pays
            adresse = urllib.parse.quote(adresse)
            longitude,latitude = getLatitudeLongitude(adresse)
            self.latitude = latitude
            self.longitude = longitude
            self.save()

    def __str__(self):
        return self.nom

    @property
    def latitudeLongitude(self):
        return [self.latitude, self.longitude]

    @latitudeLongitude.setter
    def latitudeLongitude(self, x):
        self.latitude, self.longitude = x

    @property
    def villePropre(self):
        v = self.codePostal
        m = RE_CEDEX.match(v)
        if m:
            v = m.group(1)
        return v

# Table Contact
class Contact(models.Model):
    nomContact           = models.CharField(max_length=100)
    libelleContact       = models.CharField(max_length=250)
    emailContact         = models.EmailField(max_length=100)
    entrepriseContact    = models.ForeignKey(Entreprise)

    def __str__(self):
        return self.nomContact

# Table Taxe
class Taxe(models.Model):
    dateTaxe            = models.DateField()
    entrepriseTaxe      = models.ForeignKey(Entreprise)

# Table Contacter
class Contacter(models.Model):
    expediteurContacter         = models.CharField(max_length=100)
    modaliteContacter           = models.CharField(max_length=100)
    personneContacter           = models.ForeignKey(Contact)
    entrepriseContacter         = models.ForeignKey(Entreprise)

    def __str__(self):
        return self.entrepriseContact.__str__()+" : "+ self.nomContact
