from django.conf.urls import url
from . import views
urlpatterns =[
                #Accueil
                url(r'^$',views.index, name='index'),

                #Entreprise
                url(r'^entreprises$',views.consultationEntreprise ,name='Liste des entreprises'),
                url(r'^entreprises/recherche$',views.rechercheEntreprise,name='Recherche des entreprises'),
                url(r'^entreprises/itineraire$',views.itineraireEntreprise ,name='Itinéraire entre deux entreprises'),
                url(r'^entreprise/(?P<identifiant>\d+)$',views.lireEntreprise ,name='Listing d\'une entreprise'),
                url(r'^entreprise/ajouter$',views.ajouterEntreprise,name='Ajout d\'une entreprise'),
                url(r'^entreprise/modifier/(?P<identifiant>\d+)$',views.modifierEntreprise ,name='Modification d\'une entreprise'),
                url(r'^entreprise/supprimer/(?P<identifiant>\d+)$',views.suppressionEntreprise ,name='Suppression d\'une entreprise'),

                # Contact
                url(r'^contacts$',views.consultationContact,name='Liste des contacts'),
                url(r'^contacts/recherche$',views.rechercheContact,name='Recherche des contacts'),
                url(r'^contact/ajouter$',views.ajouterContact,name='Ajout d\'un contact'),
                url(r'^contact/ajouter/(?P<identifiant>\d+)$',views.ajouterContactParEntreprise,name='Ajout d\'un contact'),
                url(r'^contact/modifier/(?P<identifiant>\d+)$',views.modifierContact ,name='Modification d\'un contact'),
                url(r'^contact/supprimer/(?P<identifiant>\d+)$',views.suppressionContact ,name='Suppression d\'un contact'),

                # Taxe
                url(r'^taxe/ajouter$',views.ajouterTaxe,name='Ajout d\'une taxe'),
                url(r'^taxe/ajouter/(?P<identifiant>\d+)$',views.ajouterTaxeParEntreprise,name='Ajout d\'une taxe'),
                url(r'^taxe/modifier/(?P<identifiant>\d+)$',views.modifierTaxe ,name='Modification d\'une taxe'),
                url(r'^taxe/supprimer/(?P<identifiant>\d+)$',views.suppressionTaxe ,name='Suppression d\'une taxe'),
       
                #Contacter
                url(r'^contacter/ajouter/(?P<identifiant>\d+)$',views.ajouterContacterParEntreprise,name='Ajout d\'un contacter'),
                url(r'^contacter/modifier/(?P<identifiant>\d+)$',views.modifierContacter ,name='Modification d\'un contacter'),
                url(r'^contacter/supprimer/(?P<identifiant>\d+)$',views.suppressionContacter ,name='Suppression d\'un contacter'),
                url(r'^contacter/afficher/(?P<identifiant>\d+)$',views.publipostage ,name='Téléchargement du message'),
                


             ]
